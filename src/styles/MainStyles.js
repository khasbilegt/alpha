import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  bg: {
    flex: 1,
    backgroundColor: "#24B2BD"
  },
  grid: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    // backgroundColor: "#28ADE1",
    margin: 20,
    marginTop: 50,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  checker: {
    flex: 5,
    margin: 10
  },
  item: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#124efc',
    margin: 5
  },
  itemFound: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid'
  },
  cell: {
    flex: 10,
    borderRadius: 10,
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    margin: 3
  },
  cellUnpressed: {
    borderColor: '#28ADE1',
    backgroundColor: '#28ADE1',
  },
  cellPressed: {
    borderColor: '#E76F8F',
    backgroundColor: '#E76F8F'
  },
  inline: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly'
  },
  letter: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 4,
    fontSize: 25,
    fontWeight: 'bold',
  },
  center: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#24B2BD"
  }
});

export default styles