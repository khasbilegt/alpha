import React, { Component } from 'react';
import {
    View,
    FlatList,
    Text
} from 'react-native';
import styles from './styles/MainStyles';

export default class WordList extends Component {
    constructor(props) {
        super(props);

        this.state = { words: [] }
    }
    getRandom = (arr, n) => {
        var result = new Array(n),
            len = arr.length,
            taken = new Array(len);
        if (n > len)
            throw new RangeError("getRandom: more elements taken than available");
        while (n--) {
            var x = Math.floor(Math.random() * len);
            result[n] = arr[x in taken ? taken[x] : x].toLowerCase();
            taken[x] = --len in taken ? taken[len] : len;
        }
        return result;
    }
    componentDidMount() {
        this.setState({
            words: this.getRandom(this.words, 5)
        }, () => {
            console.log(this.state);
        })
    }
    render() {
        return (
            <View style={styles.checker}>
                <FlatList
                    data={this.state.words}
                    renderItem={({ item }) => <Text style={[styles.item]}>{item}</Text>}
                    keyExtractor={(index) => index.toString()}
                />
            </View>
        );
    }
}