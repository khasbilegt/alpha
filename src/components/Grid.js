import React, { Component } from 'react';
import {
    TouchableOpacity,
    Text,
    View,
    Button,
    FlatList
} from 'react-native';
import styles from '../styles/MainStyles';

export default class Grid extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            table: [],
            words: [],
            word: [],
            list: [],
            unlist: [],
            wordById: []
        };
        this.codeInputRefs = [];
        this.pressedCells = [];
    }
    directioninfo = (word, direction, width, height) => {
        // determine the bounds
        let minx = 0, miny = 0;
        let maxx = width - 1;
        let maxy = height - 1;
        let dx = 0, dy = 0;
        switch (direction) {
            case 0: // up-right
                maxy = height - 1;
                miny = word.length - 1;
                dy = -1;
                maxx = width - word.length;
                minx = 0;
                dx = 1;
                break;
            case 1: // right
                maxx = width - word.length;
                minx = 0;
                dx = 1;
                break;
            case 2: // down-right
                miny = 0;
                maxy = height - word.length;
                dy = 1;
                maxx = width - word.length;
                minx = 0;
                dx = 1;
                break;
            case 3: // down
                miny = 0;
                maxy = height - word.length;
                dy = 1;
                break;
            default: /* NOTREACHED */
                break;
        }
        return {
            maxx: maxx,
            maxy: maxy,
            minx: minx,
            miny: miny,
            dx: dx,
            dy: dy
        }
    }
    wordsearch = (words, width, height) => {
        let LETTERS = 'abcdefghijklmnopqrstuvwxyz';
        let WORD_RE = /^[a-z]+$/;
        let MAXATTEMPTS = 50;

        if (!words || !words.length) return false;
        width = +width || 20;
        height = +height || 20;
        opts = {};
        opts.backwards = opts.hasOwnProperty('backwards') ? opts.backwards : 0.5;
        opts.letters = LETTERS;

        words = words.filter(function (a) {
            return WORD_RE.test(a);
        });

        words.sort((a, b) => {
            return a.length < b.length ? -1 : 1;
        });

        let grid = new Array(height);
        for (let i = 0; i < grid.length; i++)
            grid[i] = new Array(width);

        let unplaced = [];

        for (let i = 0; i < words.length; i++) {
            let word = originalword = words[i];

            if (Math.random() < opts.backwards)
                word = word.split('').reverse().join('');

            let attempts = 0;
            while (attempts < MAXATTEMPTS) {
                // determine the direction (up-right, right, down-right, down)
                let direction = Math.floor(Math.random() * 4);
                let info = this.directioninfo(word, direction, width, height);

                // word is too long, bail out
                if (info.maxx < 0 || info.maxy < 0 || info.maxy < info.miny || info.maxx < info.minx) {
                    unplaced.push(originalword);
                    break;
                }

                // random starting point
                let x = ox = Math.round(Math.random() * (info.maxx - info.minx) + info.minx);
                let y = oy = Math.round(Math.random() * (info.maxy - info.miny) + info.miny);

                // check to make sure there are no collisions
                let placeable = true;
                let count = 0;
                for (let l = 0; l < word.length; l++) {
                    let charingrid = grid[y][x];

                    if (charingrid) {
                        if (charingrid !== word.charAt(l)) {
                            placeable = false;
                            break;
                        } else {
                            count++;
                        }
                    }
                    y += info.dy;
                    x += info.dx;
                }
                if (!placeable || count >= word.length) {
                    attempts++;
                    continue;
                }

                x = ox;
                y = oy;
                for (let l = 0; l < word.length; l++) {
                    grid[y][x] = word.charAt(l);

                    y += info.dy;
                    x += info.dx;
                }
                break;
            } // end placement while loop

            if (attempts >= 20) unplaced.push(originalword);
        }

        let solved = JSON.parse(JSON.stringify(grid));

        // put in filler characters
        for (let i = 0; i < grid.length; i++)
            for (let j = 0; j < grid[i].length; j++)
                if (!grid[i][j]) {
                    solved[i][j] = ' ';
                    grid[i][j] = opts.letters.charAt(
                        Math.floor(Math.random() * opts.letters.length)
                    );
                }

        return {
            grid: grid,
            solved: solved,
            unplaced: unplaced
        };
    }
    componentWillMount() {
        this.setState({
            words: this.props.words
        });
    }
    componentDidMount() {
        let table = this.wordsearch(this.state.words, 10, 10);
        let list = this.state.words;
        console.log(table.solved)
        table.unplaced.forEach(element => {
            list = (list.filter(item => item !== element))
        });

        this.setState({
            list: list
        })

        let grid = [];
        for (let i = 0; i < 10; i++) {
            let cell = [];
            for (let j = 0; j < 10; j++) {
                cell.push(
                    <Cell
                        ref={ref => { this.codeInputRefs[((i * 10) + j)] = ref; }}
                        onPress={() => this.onCellsButton((i * 10) + j)}
                        letter={table.grid[i][j]}
                        key={i * 10 + j}
                    />
                );
            }
            grid.push(<Row letters={cell} />);
        }

        this.setState({
            table: grid,
        });
    }
    onCellsButton = async (index) => {
        let returnValue = await this.codeInputRefs[index]._onPressButton();

        if (returnValue.state) {
            if (this.state.wordById[this.state.wordById.length - 1] !== index) {
                this.setState(prevState => ({
                    wordById: [...prevState.wordById, index],
                    word: [...prevState.word, returnValue.letter]
                }), () => {
                    console.log(returnValue);
                });

                this.pressedCells.push(index);
            }
        }
    }
    onCheckButton = () => {
        console.log(this.state)
        this.state.list.forEach(word => {
            if (word === this.state.word.join('')) {
                let list = this.state.list
                list = (list.filter(item => item !== word))

                this.state.wordById.forEach(element => {
                    this.pressedCells = (this.pressedCells.filter(item => item !== element))
                });

                this.setState(prevState => ({
                    unlist: [...prevState.unlist, word],
                    list: list,
                    word: [],
                    wordById: []
                }), () => {
                    console.log(this.state);
                })
            }
        });
    }
    onClearButton = () => {
        this.pressedCells.forEach(index => {
            this.codeInputRefs[index]._onClear();
        });
        this.setState({
            word: [],
            wordById: [],
        }, () => {
            console.log(this.state);
        })
    }
    randomLetter = () => {
        const letters = 'abcdefghijklmnopqrstuvwxyz';
        let i = letters.length - 1;
        const j = Math.floor(Math.random() * i);
        return letters[j];
    }
    render() {
        return (
            <View style={styles.grid}>
                {this.state.table.map((item, index) => {
                    return (<Row key={index} letters={item} />);
                })}
                <View style={styles.inline}>
                    <Button
                        onPress={this.onCheckButton}
                        title="Check"
                        color="#841584"
                        style={styles.btn}
                    />
                    <Button
                        onPress={this.onClearButton}
                        title="Clear"
                        color="#841584"
                        style={styles.btn}
                    />
                </View>
                <View style={styles.checker}>
                    <FlatList
                        data={this.state.list}
                        renderItem={({ item }) => <Text style={[styles.item]}>{item}</Text>}
                        keyExtractor={(index) => index.toString()}
                    />
                    <FlatList
                        data={this.state.unlist}
                        renderItem={({ item }) => <Text style={[styles.item, styles.itemFound]}>{item}</Text>}
                        keyExtractor={(index) => index.toString()}
                    />
                </View>
            </View>
        );
    }
}
class Row extends Component {
    render() {
        return (
            <View style={styles.row}>{this.props.letters}</View>
        );
    }
}
class Cell extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pressed: false,
        }
    }
    _onPressButton = async () => {
        await this.setState(prevState => ({
            pressed: !prevState.pressed
        }));
        return ({
            state: this.state.pressed,
            letter: this.props.letter,
        });
    }
    _onClear = async () => {
        await this.setState({
            pressed: false
        });
    }
    render() {
        return (
            <TouchableOpacity style={
                this.state.pressed
                    ? [styles.cell, styles.cellPressed]
                    : [styles.cell, styles.cellUnpressed]
            }>
                <Text style={styles.letter} onPress={this.props.onPress}>
                    {this.props.letter}
                </Text>
            </TouchableOpacity>
        );
    }
}