import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
} from 'react-native';
import styles from './styles/MainStyles';
import Grid from './components/Grid';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = { isLoading: true, words: [] }
    this.words = ["Absence", "Accelerate", "Acknowledge", "Affair", "Bind", "Confusion", "Detest", "Distinct", "Excitement", "Gene", "Pollution", "Process", "Quantity", "Relationship", "Remote", "Routine", "Sentence", "Significantly", "Sudden", "Supply"]
  }
  getRandom = (arr, n) => {
    var result = new Array(n),
      len = arr.length,
      taken = new Array(len);
    if (n > len)
      throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
      var x = Math.floor(Math.random() * len);
      result[n] = arr[x in taken ? taken[x] : x].toLowerCase();
      taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
  }
  componentDidMount() {
    this.setState({
      isLoading: false,
      words: this.getRandom(this.words, 5)
    })
  }
  _onPressButton = () => {
    let returnValue = this.refs.itsGrid.onCellsButton(1);
    console.log('end yu baina --- ', returnValue);
  }
  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.center}>
          <ActivityIndicator />
        </View>
      )
    }
    return (
      <View style={styles.bg}>
        <Grid
          ref={'itsGrid'}
          onCellsButton={() => this._onPressButton()}
          words={this.state.words}
        />
      </View>
    );
  }
}